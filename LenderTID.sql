USE [BNYM_TrackingTool]
GO
/****** Object:  Table [dbo].[LenderTID]    Script Date: 8/9/2022 8:31:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LenderTID](
	[PID] [bigint] NOT NULL,
	[LenderName] [varchar](100) NULL,
	[ExpectedLoans] [varchar](100) NULL,
	[BaileeName] [varchar](100) NULL
) ON [PRIMARY]
GO
